" Easy CAPS
" inoremap <c-u> <ESC>viwUi         " Insert mode
" nnoremap <c-u> viwU<Esc>          " Normal mode

" Alternate way to save
nnoremap <C-s> :w<CR>

" Alternate way to quit
nnoremap <C-q> :q!<CR>

" Open startup screen
" nnoremap <C-q> :Startify<CR>

" Use control-c instead of escape
nnoremap <C-c> <Esc>

" Delete word after cursor
imap <C-d> <C-[>diwi

" Better tabbing - press twice
vnoremap < <gv
vnoremap > >gv

" Buffers dialog
map <C-e> :Buffers<CR>

" Next / previous buffers navigation
function! SwitchToNextBuffer(incr)
  let help_buffer = (&filetype == 'help')
  let current = bufnr("%")
  let last = bufnr("$")
  let new = current + a:incr
  while 1
    if new != 0 && bufexists(new) && ((getbufvar(new, "&filetype") == 'help') == help_buffer)
      execute ":buffer ".new
      break
    else
      let new = new + a:incr
      if new < 1
        let new = last
      elseif new > last
        let new = 1
      endif
      if new == current
        break
      endif
    endif
  endwhile
endfunction

" Next / previous buffer
nnoremap <silent> <C-PageUp> :call SwitchToNextBuffer(-1)<CR>
nnoremap <silent> <C-PageDown> :call SwitchToNextBuffer(1)<CR>

" Close current buffer - not working ATM, need more work
nnoremap <C-w> :bd<CR>

" Close current buffer
" nnoremap <C-S-w> :1,$bd!<CR>

function! TwiddleCase(str)
  if a:str ==# toupper(a:str)
    let result = tolower(a:str)
  elseif a:str ==# tolower(a:str)
    let result = substitute(a:str,'\(\<\w\+\>\)', '\u\1', 'g')
  else
    let result = toupper(a:str)
  endif
  return result
endfunction

" Shift + ` to change case
vnoremap ~ y:call setreg('', TwiddleCase(@"), getregtype(''))<CR>gv""Pgv