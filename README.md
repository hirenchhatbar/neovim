# neovim

## Installation

- Install vim-plug (https://github.com/junegunn/vim-plug)
- Install intelephense (https://www.npmjs.com/package/intelephense) (https://intelephense.com)
- Setup soft link if required:
```
ln -s /usr/share/nvm/versions/node/v14.18.2/bin/intelephense /usr/local/bin/intelephense
```
- Install ctags (https://ctags.io)
- Run :PlugInstall
- Enjoy!

## GUI client
- Neovide (https://github.com/neovide/neovide)
- Startup command line:
```
neovide %F --maximized
```

## ToDo

- Auto completion enhancements - make arrow keys working, highlight function signature and description in tooltip
- Auto completion of local project codebase
- Tagbar in floating window
- Terminal in floating window