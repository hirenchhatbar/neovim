source $HOME/mydata/neovim/settings.vim

source $HOME/mydata/neovim/mappings.vim

source $HOME/mydata/neovim/plugins/vim-plug.vim
source $HOME/mydata/neovim/plugins/fzf.vim
source $HOME/mydata/neovim/plugins/vim-airline.vim
source $HOME/mydata/neovim/plugins/vim-startify.vim


" https://github.com/preservim/tagbar - class outline viewer
source $HOME/mydata/neovim/plugins/tagbar.vim

source $HOME/mydata/neovim/plugins/kanagawa.vim
source $HOME/mydata/neovim/plugins/vim-which-key.vim

" https://github.com/nvim-treesitter/nvim-treesitter - highlighting
source $HOME/mydata/neovim/plugins/nvim-treesitter.vim


" https://github.com/neovim/nvim-lspconfig - language support
source $HOME/mydata/neovim/plugins/nvim-lspconfig.vim


" https://github.com/hrsh7th/nvim-cmp - auto completion
source $HOME/mydata/neovim/plugins/nvim-cmp.vim

source $HOME/mydata/neovim/plugins/vim-floaterm.vim