call plug#begin("~/.vim/plugged")
  " Better comments
  Plug 'tpope/vim-commentary'

  " Change dates fast
  Plug 'tpope/vim-speeddating'

  " Convert binary, hex, etc..
  Plug 'glts/vim-radical'

  " Repeat stuff
  Plug 'tpope/vim-repeat'

  " Text navigation
  Plug 'unblevable/quick-scope'

  " Plugin Section
  Plug 'dracula/vim', { 'as': 'dracula' }

  " Easymotion
  Plug 'easymotion/vim-easymotion'

  " Navigation
"  Plug 'justinmk/vim-sneak'

  " Surround
  Plug 'tpope/vim-surround'

  " Have the file system follow you around
  Plug 'airblade/vim-rooter'

  " Auto set indent settings
  Plug 'tpope/vim-sleuth'

  " Better syntax support
  Plug 'sheerun/vim-polyglot'

  " Treesitter
"  Plug 'nvim-treesitter/nvim-treesitter'
"  Plug 'nvim-treesitter/playground'

  " Cool icons
  Plug 'kyazdani42/nvim-web-devicons'
  Plug 'ryanoasis/vim-devicons'

  " Auto pairs for '(' '[' '{'
  Plug 'jiangmiao/auto-pairs'

  " Closetags
  Plug 'alvan/vim-closetag'

  " FZF
  Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
  Plug 'yuki-ycino/fzf-preview.vim', { 'branch': 'release/rpc', 'do': ':UpdateRemotePlugins' }
  Plug 'junegunn/fzf.vim'

  " Terminal
  Plug 'voldikss/vim-floaterm'

  " Start screen
  Plug 'mhinz/vim-startify'

  " Vista
  Plug 'liuchengxu/vista.vim'

  " See what keys do like in emacs
  Plug 'liuchengxu/vim-which-key'

  " Zen mode
  Plug 'junegunn/goyo.vim'

  " Snippets
  Plug 'honza/vim-snippets'
  Plug 'mattn/emmet-vim'

  " Interactive code
  Plug 'metakirby5/codi.vim'

  " Better tabline
"  Plug 'romgrk/barbar.nvim'

  " Undo time travel
  Plug 'mbbill/undotree'

  " Find and replace
  Plug 'ChristianChiarulli/far.vim'

  " Auto change html tags
  Plug 'AndrewRadev/tagalong.vim'

  " Smooth scroll
"  Plug 'psliwka/vim-smoothie'

  " Intuitive buffer closing
  Plug 'moll/vim-bbye'

  " Intellisense
  Plug 'neoclide/coc.nvim', {'branch': 'release'}

  Plug 'machakann/vim-sandwich'

"  Plug 'scrooloose/nerdtree'

  " Status line
"  Plug 'glepnir/galaxyline.nvim'
"  Plug 'kevinhwang91/rnvimr'

  Plug 'bling/vim-airline'
  Plug 'vim-airline/vim-airline-themes'

"  Plug 'majutsushi/tagbar'

  Plug 'Yggdroot/indentLine'

  " Colorizer
  Plug 'norcalli/nvim-colorizer.lua'
  Plug 'junegunn/rainbow_parentheses.vim'

  " Git
  Plug 'airblade/vim-gitgutter'
  Plug 'tpope/vim-fugitive'
  Plug 'tpope/vim-rhubarb'
  Plug 'junegunn/gv.vim'
  Plug 'rhysd/git-messenger.vim'

  " Swap windows
  Plug 'wesQ3/vim-windowswap'
call plug#end()

" Automatically install missing plugins on startup
autocmd VimEnter *
  \  if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
  \|   PlugInstall --sync | q
  \| endif
