" source $HOME/mydata/neovim/plugins/configs/polyglot.vim
" console.log("🚀 ~ file: init.vim ~ line 2 ~ source", source)

" source $HOME/mydata/neovim/plugins/plugins.vim

source $HOME/mydata/neovim/settings/general.vim

" source $HOME/mydata/neovim/plugins/configs/vim-which-key.vim
" source $HOME/mydata/neovim/plugins/configs/vim-easymotion.vim
" source $HOME/mydata/neovim/plugins/configs/nerdtree.vim
" source $HOME/mydata/neovim/plugins/configs/vim-devicons.vim
" source $HOME/mydata/neovim/plugins/configs/vim-airline.vim
" source $HOME/mydata/neovim/plugins/configs/tagbar.vim
" source $HOME/mydata/neovim/plugins/configs/emmet-vim.vim
" source $HOME/mydata/neovim/plugins/configs/fzf.vim
" source $HOME/mydata/neovim/plugins/configs/vim-rooter.vim
" source $HOME/mydata/neovim/plugins/configs/floaterm.vim
" source $HOME/mydata/neovim/plugins/configs/vim-closetag.vim
" source $HOME/mydata/neovim/plugins/configs/rnvimr.vim
" source $HOME/mydata/neovim/plugins/configs/vim-startify.vim
" source $HOME/mydata/neovim/plugins/configs/goyo.vim
" source $HOME/mydata/neovim/plugins/configs/codi.vim
" source $HOME/mydata/neovim/plugins/configs/barbar.vim
" source $HOME/mydata/neovim/plugins/configs/far.vim
" source $HOME/mydata/neovim/plugins/configs/tagalong.vim
" source $HOME/mydata/neovim/plugins/configs/quick-scope.vim
" source $HOME/mydata/neovim/plugins/configs/vista.vim
" source $HOME/mydata/neovim/plugins/configs/rainbow-parentheses.vim
" source $HOME/mydata/neovim/plugins/configs/git-messenger.vim
" source $HOME/mydata/neovim/plugins/configs/vim-gitgutter.vim
" source $HOME/mydata/neovim/plugins/configs/rnvimr.vim

" source $HOME/mydata/neovim/plugins/configs/coc/coc-explorer.vim
" source $HOME/mydata/neovim/plugins/configs/coc/coc-extensions.vim
" source $HOME/mydata/neovim/plugins/configs/coc/coc-phpls.vim
" source $HOME/mydata/neovim/plugins/configs/sneak.vim

" luafile $HOME/mydata/neovim/lua/nvim-colorizer.lua
" luafile $HOME/mydata/neovim/lua/nvcodeline.lua
" luafile $HOME/mydata/neovim/lua/treesitter.lua

source $HOME/mydata/neovim/keys/mappings.vim

" source $HOME/mydata/neovim/themes/dracula.vim
