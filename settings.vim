let g:mapleader = "\;"      " Set leader key

syntax on                   " Syntax highlighting

set nocompatible            " Disable compatibility to old-time vi
set showmatch               " Show matching 
set ignorecase              " Case insensitive 
set mouse=v                 " Middle-click paste with 
set hlsearch                " Highlight search 
set incsearch               " Incremental search
set tabstop=4               " Number of columns occupied by a tab 
set softtabstop=4           " See multiple spaces as tabstops so <BS> does the right thing
set expandtab               " Converts tabs to white space
set shiftwidth=4            " Width for autoindents
set autoindent              " Indent a new line the same amount as the line just typed
set number                  " Add line numbers
set wildmode=longest,list   " Get bash-like tab completions
set mouse=a                 " Enable mouse click
set clipboard=unnamedplus   " Using system clipboard
set cursorline              " Highlight current cursorline
set ttyfast                 " Speed up scrolling in Vim
set hidden                  " Required to keep multiple buffers open multiple buffers
set nowrap                  " Display long lines as just one line
set encoding=utf-8          " The encoding displayed
set pumheight=10            " Makes popup menu smaller
set fileencoding=utf-8      " The encoding written to file
set ruler                   " Show the cursor position all the time
set cmdheight=2             " More space for displaying messages
set iskeyword+=-            " treat dash separated words as a word text object"
set splitbelow              " Horizontal splits will automatically be below
set splitright              " Vertical splits will automatically be to the right
set t_Co=256                " Support 256 colors
set conceallevel=0          " So that I can see `` in markdown files
set smarttab                " Makes tabbing smarter will realize you have 2 vs 4
set smartindent             " Makes indenting smart
set laststatus=0            " Always display the status line
set background=dark         " tell vim what the background color looks like
set showtabline=2           " Always show tabs
set updatetime=300          " Faster completion
set timeoutlen=500          " By default timeoutlen is 1000 ms
set formatoptions-=cro      " Stop newline continution of comments

set nobackup                " This is recommended by coc
set nowritebackup           " This is recommended by coc

" set cc=80                   " Set an 80 column border for good coding style

filetype plugin indent on   " Allow auto-indenting depending on file type
filetype plugin on

set termguicolors

set guifont=Hack:h10

" Neovide
let g:neovide_fullscreen=v:true

set completeopt=menu,menuone,noselect

au! BufWritePost $MYVIMRC source %      " auto source when writing to init.vm alternatively you can run :source $MYVIMRC